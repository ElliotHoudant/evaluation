

-- 1) Afficher les patients par ordre croissant de date de naissance.

select *
from patient p
order by date_naissance 

-- 2) Combien y a t'il de patients femme (sexe = 2) ?

select count(id_patient) 
from patient p
where sexe ='2'


-- 3) Combien de patients sont nés avant le 01/01/1930 ?

select count(id_patient) 
from patient p 
where date_naissance < '01/01/1930'

-- 4) Combien de séjour durent plus de 10 jours (> 10) ?

select count(id_sejour)
from sejour s 
where date_sortie - date_entree > 10

-- 5) Afficher les idenfiants des patients qui n'ont qu'un seul séjour ?

select count(id_patient) as nmb,id_patient
from sejour s inner join patient p 
using (id_patient)
group by (id_patient)
order by id_patient 




-- 6) Combien y a-t'il de RUMs dans l'UF ME15 ?

select count(id_rum) 
from rum r 
where code_uf = 'ME15'

-- 7) Affichier les codes UF dans lesquelles est passé le patient 10 ?

select code_uf
from sejour s inner join patient p 
using (id_patient)
inner join rum r 
using(id_sejour)
where id_patient ='10'

-- 8) Afficher tous les RUMs qui ne se sont pas déroulés dans l'UF ME12.

select id_rum 
from rum
where rum.code_uf != 'ME12'

-- 9) Afficher le nombre de RUMs par code UF et par nombre décroissant.

select count(code_uf), code_uf 
from rum
group by code_uf 
order by count desc  


-- 10) Afficher l'âge à l'admission du séjour



-- 11) Bonus : Combien de patients avaient plus de 90 ans à l'admission ?






